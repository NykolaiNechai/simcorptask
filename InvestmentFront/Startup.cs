﻿using AutoMapper;
using InvestmentFront.Domain.Abstraction;
using InvestmentFront.Domain.Concreaty;
using InvestmentFront.Domain.Entities;
using InvestmentFront.Infrastructure.BUS;
using InvestmentFront.Infrastructure.BusinessLogic;
using InvestmentFront.Infrastructure.Services;
using InvestmentFront.Utils.Mappings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace InvestmentFront
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<LoanRequestToLoanProfile>();
                cfg.AddProfile<ProductInfoToLoanRequestProfile>();
                cfg.AddProfile<ProductToProductInfoProfile>();
                cfg.AddProfile<ScheduleViewModelToScheduleDtoProfile>();
            });
            services.AddAutoMapper();

            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IRepository<Loan>, LoanRepository>();
            services.AddScoped<IRepository<LoanRequest>, LoanRequestRepository>();
            services.AddScoped<IRepository<LoanTransaction>, LoanTransactionRepository>();
            services.AddScoped<IRangeCalculator, RangeCalculator>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICommandHandler, LoanRequestHandler>();
            services.AddScoped<ICommandHandler, LoanHandler>();
            services.AddScoped<ICommandHandler, LoanCalculationHandler>();
            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<ICalculationService, CalculationService>();

            services.AddMemoryCache();
            services.AddSession();
            services.AddMvc();
            services.AddHttpContextAccessor();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();

            } else {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSession();

            app.UseMvc(routes => {
                routes.MapRoute(name: "Error", template: "Error",
                     defaults: new { controller = "Error", action = "Error" });

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
        });
        }
    }
}
