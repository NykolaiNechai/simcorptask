﻿namespace InvestmentFront.Domain.Entities
{
    public enum LoanStatus
    {
        Created = 1, Accepted, Rejected, Postpone, Closed, Debt
    }
}
