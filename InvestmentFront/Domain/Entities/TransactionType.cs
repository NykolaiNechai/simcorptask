﻿namespace InvestmentFront.Domain.Entities
{
    public enum TransactionType
    {
        In=1, Out, WriteOff
    }
}
