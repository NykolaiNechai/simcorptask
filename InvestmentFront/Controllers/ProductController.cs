﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using InvestmentFront.Domain.Abstraction;
using InvestmentFront.Domain.Entities;
using InvestmentFront.Infrastructure.BusinessLogic;
using InvestmentFront.Infrastructure.Services;
using InvestmentFront.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InvestmentFront.Controllers
{
    public class ProductController : Controller
    {
        public IProductService _productService{ get; }

        public ProductController(IProductService productService) {
            _productService = productService;
        }

        [HttpGet]
        public IActionResult GetProducts() {
            var products = _productService.GetProducts();
            return View(products);
        }
    }
}