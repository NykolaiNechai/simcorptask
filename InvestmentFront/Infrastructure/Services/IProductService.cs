﻿using AutoMapper;
using InvestmentFront.Domain.Abstraction;
using InvestmentFront.Domain.Entities;
using InvestmentFront.Infrastructure.BusinessLogic;
using InvestmentFront.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InvestmentFront.Infrastructure.Services
{
    public interface IProductService
    {
        IEnumerable<ProductInfo> GetProducts();
    }
}
